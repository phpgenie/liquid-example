<?php
define('LIQUID_INCLUDE_SUFFIX', 'liquid');
define('LIQUID_INCLUDE_PREFIX', '');

require_once('liquid/Liquid.class.php');

define('PROTECTED_PATH', dirname(__FILE__).'/protected/');

$liquid = new LiquidTemplate();

$products = array(
	'products' => array(
		array(
			'title' => 'Product 1',
			'price' => '1200',
			'description' => 'This is a product. There are many like it but this product is mine.',
		),
		array(
			'title' => 'Test Product 2',
			'price' => '42234',
			'description' => 'This is the second product',
		),
		array(
			'title' => 'Test Product 3',
			'price' => '1000',
			'description' => 'This is the third product',
		),
	),
);

$liquid->parse(file_get_contents('products.liquid'));

print $liquid->render($products);

?>