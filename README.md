# Liquid Example

Just a simple example that uses a [liquid package](https://github.com/harrydeluxe/php-liquid).

Run using `php index.php` in the command line from the root of the directory.

You should get he following output:

```html
<ul id="products">
  
    <li>
      <h2>Product 1</h2>
      Only 1200

      <p>This is a product. There are many like it but this product is mine.</p>

    </li>
  
    <li>
      <h2>Test Product 2</h2>
      Only 42234

      <p>This is the second product</p>

    </li>
  
    <li>
      <h2>Test Product 3</h2>
      Only 1000

      <p>This is the third product</p>

    </li>
  
</ul>
```